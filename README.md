# Turkish Coreference Annotation Manual 

This repository contains the source code of the
Turkish Coreference Annotation Manual 

# History

* Version 2 extended by Kübra Cıngıllı and Ferit Tunçer (December 1, 2016)

* Version 1 by Barış Gün Sürmeli and Peter Schüller (August 27, 2015)

# Citation

* Barış Gün Sürmeli, Kübra Cıngıllı, Ferit Tunçer, and Peter Schüller.
  Turkish Coreference Annotation Manual (V2). 2016.
  https://tinyurl.com/jovfmua 

* Barış Gün Sürmeli and Peter Schüller.
  Turkish Coreference Annotation Manual (V1). 2015.
  https://tinyurl.com/z7xow7v

# License

  This work is licensed under the Creative Commons Attribution 4.0 International License.
  To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
  or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

How to build:

  $ pdflatex guide.tex
  
