\documentclass[a4paper,11pt]{article}
\usepackage{paralist}
\usepackage{geometry}
\usepackage{todonotes}
\usepackage{titlesec}
\usepackage[normalem]{ulem}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}

\geometry{margin=2cm,top=1.5cm}

%\setmainlanguage{english}
\setlength{\parindent}{0mm}
\setlength{\parskip}{3pt}

\titlespacing{\section}{0pt}{4pt}{4pt}
\titlespacing{\subsection}{0pt}{0pt}{0pt}
\titlespacing{\subsubsection}{0pt}{0pt}{0pt}
\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\large\bfseries}{\thesubsection}{1em}{}
\titleformat{\subsubsection}{\bfseries}{\thesubsubsection}{1em}{}

\renewcommand{\emph}[1]{\textbf{#1}}
\newcommand{\myexample}[1]{\par\smallskip\quad\begin{minipage}{0.9\textwidth}``#1''\end{minipage}\par\smallskip}

\pagestyle{fancy}
\lhead{}
\chead{}
\rhead{}
\rfoot{}
\cfoot{\thepage/2}
\rfoot{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\newcounter{principle}
\newcommand{\marginref}{\marginpar{\stepcounter{principle}(P\theprinciple)}}

\begin{document}

% make the footnote a star
\renewcommand*{\thefootnote}{\fnsymbol{footnote}}
\centerline{\huge Turkish Coreference Annotation Manual%
\footnote{This work was supported by the
Scientific and Technological Research Council of Turkey
(TUBITAK) Grant 114E430.}}
% continue with normal footnotes
\renewcommand*{\thefootnote}{\arabic{footnote}}
\setcounter{footnote}{0}
\bigskip
\centerline{\Large Barış Gün Sürmeli \quad Kübra Cıngıllı \quad Ferit Tunçer \quad Peter Sch\"uller}
\smallskip
\centerline{Computer Engineering Department, Faculty of Engineering}
\centerline{Marmara University, Istanbul, Turkey}
\medskip
\centerline{\small 1.12.2016 (Version 2)}
\bigskip

% the existing turkish format does not split off possessives, so it gives no additional value
% -> won't use turkish derivation-boundary conll format
%\todo[inline]{expand to turkish conll format (annotate parts of words)}

We say these parts of a text are \emph{coreferent}
because they \emph{point to the same thing} in the world.
%
Computers can learn how to find coreference automatically if we provide \emph{training data}.
%
Such training data contains text plus
\emph{manually created annotations} that show which parts of the text are coreferent.
%
This is a guide for creating such coreference annotations for Turkish.
%
As an example,
a computer cannot easily know that
``Atatürk'', ``Mustafa Kemal'', and ``Türkiye Cumhuriyeti'nin kurucusu''
refers to the same person.
%
Annotation principles are numbered in the margin as (P$x$).

Coreference annotation consists of two parts:
(i) marking \emph{mentions}: mentions are parts of the text that can be coreferent; afterwards
(ii) storing all phrases that point to the same object or being
into a \emph{coreference chain}.
In practice (i) requires to mark all phrases,
and (ii) requires to select a set of mentions marked in (i)
and storing them as a chain by giving it a name.

\section{Which parts of the text are mentions that can be coreferent?}

Noun phrases, pronouns, and nominalized adjectives are candidates for coreference.
\marginref
\smallskip

\begin{compactitem}
\item
  A noun phrase is a sequence of words that specifies a concrete entity, for example names and descriptions:
  ``Atatürk'', ``arabam'', ``benim çok şirin ama nerdeyse her zaman bozuk olan \ldots\ arabam''.
\item
  Pronouns (zamirler) are ``ben'', ``sen'', ``o'', ``biz'', \ldots, ``onun'', \ldots, ``onca'', ``odur'', \ldots
\item
  Nominalized adjectives (sıfat) are ``yenisi'', ``eskisi'', \ldots
\end{compactitem}

\section{Which coreferences should be annotated?}

All cases where two phrases point to the same concrete entity.
Also names that are used repeatedly (``Ankara", ``Ankara'da", ``Ankara") are annotated.
\marginref

\subsection{Nouns phrases with pronouns}

The most basic coreference is between nouns and pronouns.

\myexample{\uline{Ahmet} \uwave{okula} gitti. \uline{O}, \uwave{orayı} çok sevdi.}

Here the noun phrase ``Ahmet'' and the pronoun ``O'' should be marked as coreferent.
Also ``okul'' and ``orayı'' should be marked as coreferent.

\myexample{\uline{Bizim araba} eski, sizinkisi \uline{bizimkisinden} yeni.}

The noun phrase ``Bizim araba'' and the pronoun ``bizimkisinden'' are coreferent.

\emph{Important:} we do not split ``bizimkisinden'', we mark the \emph{complete word}.
%even though marking only ``bizimkisi'' can seem more logical.

\subsection{Noun phrases with noun phrases}

\subsubsection{Proper and common nouns}

If one noun phrase is a proper noun (name) and the other is a common noun
and they clearly refer to same object or being, they are coreferent.
\marginref

\myexample{\uline{Ahmet Hoca} evlendi. Umarım çok mutlu olur, \uline{profesörümüz} her şeyin en iyisini hak ediyor.}

``Ahmet Hoca'' and ``profesörümüz'' refers to same person.
%the first is a proper noun and the second is a common noun.

\subsubsection{Different proper nouns}

If two different proper nouns (names) refer to same object or being,
they are coreferent.
\marginref
%This often happens when a person is called with full name or surname or with a title.

\myexample{\uline{Prof.\ Dr.\ Kemal Doe} teşrif etti. \uline{Kemal Bey} bize çalışmalarının gidişatından bahsetti.}

%Here ``Prof.\ Dr.\ Kemal Doe'' and ``Kemal Bey'' should be marked as coreferent.

\subsection{Nominalized adjectives and verbs}

\subsubsection{Nominalized Adjectives (adlaşmış sıfat)}

\myexample{\uline{Yeni araba} ile \uwave{eski araba} karşı karşıya geldi. \uline{Yenisi}, \uwave{eskisinden} çok daha hızlıydı.}

Here noun phrase ``Yeni araba'' is coreferent with nominialized adjective ``yenisi''
and noun phrase ``eski araba'' is coreferent with ``eskisi''.
%Both coreferences should be marked.

\subsubsection{Nominalized Verbal Adjectives (adlaşmış sıfat-fiil)}

\myexample{\uline{Karşıdan gelen adam} emin adımlarla yürüyordu. Mustafa: `\uline{Geleni} tanıyorum.' Dedi.}

Here noun phrase ``Karşıdan gelen adam'' is coreferent with ``Geleni''.

\subsection{Examples and Special Cases}

\myexample{\uline{Mustafa} kendine gel. \uline{Sen} buralara gelmek için çok çalıştın. \uline{Sana} bu salmışlık hali yakışmıyor.''  --- ``\uline{Ben} de farkındayım. Düzeleceğim.}

All underlined words refer to the same person.
%and should be stored into the same co-reference chain.

\emph{Important:} ``farkındayım'' and ``Düzeleceğim'' contain references to Mustafa (-yım and -im),
BUT these words refer to what Mustafa is doing, they are NOT coreferent!
\marginref

\myexample{\uline{Kristof Kolomb'un}, öyle insanlık adına keşiflere çıkmış bir seyyah değil,
  \uline{yeni zenginlikler peşinde koşan ve tayfasına kan kusturan zalim bir kaptan} olduğunu fark ediyorsunuz.}

Here, the name and a long proper noun are coreferent.

\emph{Important:} we take the largest possible coreferent mention
(marking only ``kaptan'' would be wrong).
\marginref

\myexample{\ldots\ \uline{iktidar arayışıyla ilgili yazı yazma krizine} girmişken
  dün akşamüstü icat ettim \ldots\
  Başlığı bulunca rahatladım, \uline{krizden} kurtuldum \ldots}

Here the first coreferent is a long noun phrase that is part of the subject.

\newcommand\deeperuline{\bgroup\ULdepth=1000pt\markoverwith{\rule[-1.1ex]{2pt}{0.4pt}}\ULon}

\myexample{%
\deeperuline{\uwave{T.\ C.}\ başbakanı}\ 
Ankara'ya gitti,
\deeperuline{onun} katılımıyla 
\uwave{Cumhuriyetin}
95'inci yıldönümü kutlamaları\strut\ başladı.}

A mention can contain another mention.
\marginref
In this example there are 2 chains and 4 mentions.

\section{Which cases should not be annotated?}

\myexample{Big bang bir söylencedir.}

This is predication and not coreference: the purpose of the sentence is to say that
``bir söylencedir'' is the property of ``Big bang''.
\marginref

\myexample{Boş, kiralık apartman dairesi, bir ev değildir; o, kiralanması beklenen bir konuttur.}

``Boş, kiralık apartman dairesi'' is not a specific apartment.
There is not coreference with ``o''.
\marginref

\myexample{Derin adamdı. Pek konuşmazdı.}

The subjects of these sentences are coreferent, but they are not tokens, so we cannot annotate them.
\marginref

%\myexample{Bilim hayatın her alanında rehber alınmalıdır.
%   \uline{Bilim} ufkumuzu açmıştır, açacaktır. \uline{O} geleceğe ışık tutmaktadır \ldots}
%
%The first ``bilim'' is science in general,
%then second one is a more specific reference.
%Only the second ``bilim'' is coreferent.

\myexample{Arının peteği, arıdan arıya, kırlangıcın yuvası, kırlangıcından kırlangıcına değişmez,
   onlar yalnızca birer barınaktır \ldots}

Collections of objects are not considered coreferent.
Therefore ``onlar'' is not annotated at all.
\marginref

\clearpage
Annotations are done in CoNLL coreference format. A few examples are as follows.

The best way to edit such files is a text editor with fixed-width font
(Word or libreoffice will \emph{not} be helpful).

%\begin{table}
%\centering
%\begin{tabular}{c c c c}
%Sentence No&Word No&Word&Coreference\\\hline
%0&1&Ahmet&(1)\\
%0&2&okula&(2)\\
%0&3&gitti&-\\
%0&4&.&-\\
%1&1&O&(3)\\
%1&2&,&-\\
%1&3&orayı&(4)\\
%1&4&çok&-\\
%1&5&sevdi&-\\
%1&6&.&-\\
%\end{tabular}
%\caption{Nouns phrases with pronouns}
%\label{np}
%\end{table}

\begin{center}
\ttfamily
\begin{tabular}{@{}rrlr@{}}
\hline
0&1&Ahmet&(1\\
0&2&Hoca&1)\\
0&3&evlendi&-\\
0&4&.&-\\
1&1&Umarım&-\\
1&2&çok&-\\
1&3&mutlu&-\\
1&4&olur&-\\
1&5&,&-\\
1&6&profesörümüz&(2)\\
1&7&her\_şeyin&-\\
1&8&en&-\\
1&9&iyisini&-\\
1&10&hak&-\\
1&11&ediyor&-\\
1&12&.&-\\
\hline
\multicolumn{4}{@{}l@{}}{1=2 Ahmet Hoca}
\end{tabular}
%
\qquad\qquad
%
\begin{tabular}{@{}rrlr@{}}
\hline
0&1&Karşıdan&(1\\
0&2&gelen&-\\
0&3&adam&1)\\
0&4&emin&-\\
0&5&adımlarla&-\\
0&6&yürüyordu&-\\
0&7&.&-\\
1&1&Mustafa&-\\
1&2&:&-\\
1&3&‘&-\\
1&4&Geleni&(2)\\
1&5&tanıyorum&-\\
1&6&.&-\\
1&7&’&-\\
1&8&Dedi&-\\
1&9&.&-\\
\hline
\multicolumn{4}{@{}l@{}}{1=2 gelen adam}
\end{tabular}
\end{center}

\begin{center}
\ttfamily
\begin{tabular}{@{}rrlr@{}}
\hline
0&1&T.C.&(1(2)\\
0&2&başbakanı&1)\\
0&3&Ankara’ya&-\\
0&4&gitti&-\\
0&5&,&-\\
0&6&onun&(3)\\
0&7&katılımıyla&-\\
0&8&Cumhuriyetin&(4)\\
0&9&95’inci&-\\
0&10&yıldönümü&-\\
0&11&kutlamaları&-\\
0&12&başladı&-\\
0&13&.&-\\
\hline
\multicolumn{4}{@{}l@{}}{1=3 T.C. Başbakanı} \\
\multicolumn{4}{@{}l@{}}{2=4 Türk Cumhuriyeti}
\end{tabular}
\end{center}

\end{document}
