#!/bin/bash

# for verbose mode, use this
#set -x

F=guide

while sleep 1; do
	# run if bib or tex newer than pdf
	doit=0
	if test \! -e $F.pdf; then
		doit=1
	fi
	for f in *.bib *.tex; do
		if test $f -nt $F.pdf; then
			doit=1
		fi
	done
	if test "x$doit" == "x1"; then
		pdflatex -interaction nonstopmode $F.tex;
		pdflatex -interaction nonstopmode $F.tex;
	fi
done
